from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render
from .forms import UploadFileForm
from .models import File
from django.views.decorators.csrf import csrf_exempt
import json
import os
import sys
import threading
from compress.compress import fun

@csrf_exempt
def main(request):
    os.system("rm -rf files")
    File.objects.all().delete()
    response = {}
    if request.method == 'POST':
        if request.headers['X-ROUTING-KEY']:
            uniqid = request.headers['X-ROUTING-KEY']
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                # file is saved
                form.save()
                files = serializers.serialize('json', File.objects.all())
                uploads = json.loads(files)
                for upload in uploads:
                    name = upload['fields']['thefile']
                    compType = upload['fields']['thetype']
                process = threading.Thread(target=fun, args=(name,uniqid))
                process.start()
                response = {
                    'detail' : 'success'
                }
            else:
                response = {
                    'detail' : 'please attach the file and select the type of compression you want'
                }
        else:
            response = {
                    'detail' : 'please add X-ROUTING_KEY id to your headers'
                }
    print(response)
    return JsonResponse(response, safe=False)
