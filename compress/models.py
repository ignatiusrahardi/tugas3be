from django.db import models

# Create your models here.
class File(models.Model):
    thetype = models.CharField(max_length=50)
    thefile = models.FileField()

    def __str__(self):
        return self.thetype
    
    def delete(self, *args, **kwargs):
        self.thefile.delete()
        super().delete(*args, **kwargs)