import zipfile
import os
import pika
import sys
import threading

def fun(name, uniqid):
    zipname = os.path.abspath(os.path.dirname(name)) + '/files/YourCompressedFile.zip'
    filename = os.path.abspath(os.path.dirname(name)) + '/files/' + name

    credentials = pika.PlainCredentials('0806444524', '0806444524')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='152.118.148.95', port='5672', virtual_host='/0806444524', credentials=credentials ))

    channel = connection.channel()

    channel.exchange_declare(exchange='1606875951', exchange_type='direct')

    compressing = threading.Thread(target=zip, args=(name,filename,zipname))
    compressing.start()
    tenPercent = 0
    num = 0
    while compressing.is_alive():
        process = int(os.path.getsize(zipname)/ os.path.getsize(filename) * 100)
        if process == 0 or process < 10:
            pass
        elif process > tenPercent:
            tenPercent += 10
            message = str(tenPercent)
            channel.basic_publish(exchange='1606875951', routing_key=uniqid, body=message)
        print(str(tenPercent) + ' %')
    if tenPercent <= 100:
        channel.basic_publish(exchange='1606875951', routing_key=uniqid, body='100')
    connection.close()

def zip(originalName, filename, zipname):
    compressing = zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED)
    compressing.write(filename, arcname=originalName)